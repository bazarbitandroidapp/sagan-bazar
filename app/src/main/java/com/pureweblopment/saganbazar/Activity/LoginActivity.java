package com.pureweblopment.saganbazar.Activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.pureweblopment.saganbazar.Global.Global;
import com.pureweblopment.saganbazar.Global.Typefaces;
import com.pureweblopment.saganbazar.Instagram.InstagramApp;
import com.pureweblopment.saganbazar.Linkedin.APIHelper;
import com.pureweblopment.saganbazar.Linkedin.LISessionManager;
import com.pureweblopment.saganbazar.Linkedin.errors.LIApiError;
import com.pureweblopment.saganbazar.Linkedin.errors.LIAuthError;
import com.pureweblopment.saganbazar.Linkedin.listeners.ApiListener;
import com.pureweblopment.saganbazar.Linkedin.listeners.ApiResponse;
import com.pureweblopment.saganbazar.Linkedin.listeners.AuthListener;
import com.pureweblopment.saganbazar.Linkedin.utils.Scope;
import com.pureweblopment.saganbazar.Notifications.FirebaseInstanceIDService;
import com.pureweblopment.saganbazar.Pdk.PDKCallback;
import com.pureweblopment.saganbazar.Pdk.PDKClient;
import com.pureweblopment.saganbazar.Pdk.PDKException;
import com.pureweblopment.saganbazar.Pdk.PDKResponse;
import com.pureweblopment.saganbazar.Pdk.PDKUser;
import com.pureweblopment.saganbazar.Pdk.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executors;

import com.pureweblopment.saganbazar.Global.SendMail;
import com.pureweblopment.saganbazar.Global.SharedPreference;
import com.pureweblopment.saganbazar.Global.StaticUtility;

import com.pureweblopment.saganbazar.R;

import de.greenrobot.event.EventBus;
import io.fabric.sdk.android.Fabric;
import okhttp3.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener,
        GoogleApiClient.OnConnectionFailedListener {

    ImageView imageviewFB, imageviewInsta, imageviewGmail, imageviewLinkedin, imageviewPinterest;
    EditText editUserID, editUserPWD, editUserEmail;
    Button btnLogin;

    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 100;
    private String user_name, userEmail, gmailId, social_type, user_fb_email, user_fb_Id, user_insta_fullname,
            user_inta_Username, instagram_Id;

    LoginButton login_button;
    private CallbackManager callbackManager;
    //FOR GET FCM TOKEN...
    private SharedPreferences sharedPreferences;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private String token = "";
    private boolean isReceiverRegistered;
    private boolean sentToken;
    String DeviceName;
    public static final String TAG = LoginActivity.class.getSimpleName();

    private InstagramApp mApp;
    private HashMap<String, String> userInfoHashmap = new HashMap<String, String>();

    TextView txtsignup, txtsignupTitle, txtForgotpwd, txtSkip;

    RelativeLayout relativeProgress;

    String strUSerEmail;

    ImageView imageLogo;
    String strLOGO = "", strFB = "", strGmail = "", strinsta = "", strLinkedink = "", strPinterest = "";
    String picUrl = null;

    CoordinatorLayout coordinator;

    Context context = LoginActivity.this;

    private EventBus eventBus = EventBus.getDefault();

    private int i = 0;
    private AlertDialog internetAlert;

    PDKClient pdkClient;
    PDKUser user;
    private static boolean DEBUG = true;
    private final String USER_FIELDS = "id,image,counts,created_at,first_name,last_name,bio";
    private String strRedirect = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);

        eventBus.register(context);
        if (getIntent() != null) {
            strRedirect = getIntent().getStringExtra("Redirect");
        }

        if (SharedPreference.GetPreference(this, Global.preferenceNameGuestUSer,
                Global.SessionId) == null) {
            SharedPreference.CreatePreference(this, Global.preferenceNameGuestUSer);
            SharedPreference.SavePreference(Global.SessionId, Global.SessionId());
        }

        computePakageHash();
        pdkClient = PDKClient.configureInstance(this, StaticUtility.PinterestApp_ID);
        pdkClient.onConnect(this);

        Initialization();
        Typeface();
        ClickListener();
        setDynamicString();
        getDeviceToken();

        /*MainActivity.SetUserInfo();*/

        ProgressBar progress = (ProgressBar) findViewById(R.id.progress);
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                progress.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
            }
        }

        /*String userID = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID);
        if (userID != null) {
            startActivity(new Intent(context, MainActivity.class));
            finish();
        }*/

        coordinator.setBackgroundColor(getResources().getColor(R.color.white));

        //region Set App Logo
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.IMG_LOGO) != "") {
            strLOGO = SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.IMG_LOGO);
            //region ImageLOGO
            try {
                URL urla = null;
            /*picUrl = picUrl.replace("[", "");
            picUrl = picUrl.replace("]", "").replace("\"", "");*/
                urla = new URL(strLOGO);
                URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                picUrl = String.valueOf(urin.toURL());
                // Capture position and set to the ImageView
                Picasso.get()
                        .load(picUrl)
                        .into(imageLogo, new Callback() {
                            @Override
                            public void onSuccess() {
                                //holder.pbHome.setVisibility(View.INVISIBLE);
                            }

                            @Override
                            public void onError(Exception e) {
                                //holder.pbHome.setVisibility(View.INVISIBLE);
                            }
                        });
            } catch (MalformedURLException e) {
                e.printStackTrace();
                //Creating SendMail object
                SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT,
                        "Getting error in LoginActivity.java When parsing url\n" + e.toString());
                //Executing sendmail to send email
                sm.execute();
            } catch (URISyntaxException e) {
                e.printStackTrace();
                //Creating SendMail object
                SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT,
                        "Getting error in LoginActivity.java When parsing url\n" + e.toString());
                //Executing sendmail to send email
                sm.execute();
            }
            //endregion
        }
        //endregion

        //region Set Social media
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Feature_List) != null) {
            try {
                JSONArray jsonArray = new JSONArray(SharedPreference.GetPreference(context,
                        Global.APPSetting_PREFERENCE, StaticUtility.Feature_List));
                if(jsonArray != null) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String social_media = null;
                        if (jsonObject.optString("slug").equalsIgnoreCase("social-media-logins")) {
                            if (jsonObject.optString("feature_type").equalsIgnoreCase("multiple")) {
                                social_media = jsonObject.optString("feature_multiple");
                            } else if (jsonObject.optString("feature_type").equalsIgnoreCase("text")) {
                                social_media = jsonObject.optString("feature_textval");
                            } else if (jsonObject.optString("feature_type").equalsIgnoreCase("single")) {
                                social_media = jsonObject.optString("feature_checkval");
                            } else if (jsonObject.optString("feature_type").equalsIgnoreCase("checkbox")) {
                                social_media = jsonObject.optString("feature_checkval");
                            } else if (jsonObject.optString("feature_type").equalsIgnoreCase("link")) {
                                social_media = jsonObject.optString("feature_textval");
                            }
                            if (social_media.contains("linkedin")) {
                                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                                        StaticUtility.IMG_Linkedin) != "") {
                                    imageviewLinkedin.setVisibility(View.VISIBLE);
                                    strLinkedink = SharedPreference.GetPreference(context,
                                            Global.APPSetting_PREFERENCE, StaticUtility.IMG_Linkedin);
                                    //region ImageLinkedink
                                    try {
                                        URL urla = null;
                                        urla = new URL(strLinkedink);
                                        URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                                        picUrl = String.valueOf(urin.toURL());
                                        // Capture position and set to the ImageView
                                        Picasso.get()
                                                .load(picUrl)
                                                .into(imageviewLinkedin, new Callback() {
                                                    @Override
                                                    public void onSuccess() {
                                                        //holder.pbHome.setVisibility(View.INVISIBLE);
                                                    }

                                                    @Override
                                                    public void onError(Exception e) {
                                                        //holder.pbHome.setVisibility(View.INVISIBLE);
                                                    }
                                                });
                                    } catch (MalformedURLException e) {
                                        e.printStackTrace();
                                        //Creating SendMail object
                                        SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT,
                                                "Getting error in LoginActivity.java When parsing url\n" +
                                                        e.toString());
                                        //Executing sendmail to send email
                                        sm.execute();
                                    } catch (URISyntaxException e) {
                                        e.printStackTrace();
                                        //Creating SendMail object
                                        SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT,
                                                "Getting error in LoginActivity.java When parsing url\n" +
                                                        e.toString());
                                        //Executing sendmail to send email
                                        sm.execute();
                                    }
                                    //endregion
                                } else {
                                    imageviewLinkedin.setVisibility(View.GONE);
                                }
                            } else {
                                imageviewLinkedin.setVisibility(View.GONE);
                            }
                            if (social_media.contains("pintrest")) {
                                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                                        StaticUtility.IMG_pinterest) != "") {
                                    imageviewPinterest.setVisibility(View.VISIBLE);
                                    strPinterest = SharedPreference.GetPreference(context,
                                            Global.APPSetting_PREFERENCE, StaticUtility.IMG_pinterest);
                                    try {
                                        URL urla = null;
                                        urla = new URL(strPinterest);
                                        URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                                        picUrl = String.valueOf(urin.toURL());
                                        // Capture position and set to the ImageView
                                        Picasso.get()
                                                .load(picUrl)
                                                .into(imageviewPinterest, new Callback() {
                                                    @Override
                                                    public void onSuccess() {
                                                        //holder.pbHome.setVisibility(View.INVISIBLE);
                                                    }

                                                    @Override
                                                    public void onError(Exception e) {
                                                        //holder.pbHome.setVisibility(View.INVISIBLE);
                                                    }
                                                });
                                    } catch (MalformedURLException e) {
                                        e.printStackTrace();
                                        //Creating SendMail object
                                        SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT,
                                                "Getting error in LoginActivity.java When parsing url\n" +
                                                        e.toString());
                                        //Executing sendmail to send email
                                        sm.execute();
                                    } catch (URISyntaxException e) {
                                        e.printStackTrace();
                                        //Creating SendMail object
                                        SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT,
                                                "Getting error in LoginActivity.java When parsing url\n" +
                                                        e.toString());
                                        //Executing sendmail to send email
                                        sm.execute();
                                    }
                                    //endregion
                                } else {
                                    imageviewPinterest.setVisibility(View.GONE);
                                }
                            } else {
                                imageviewPinterest.setVisibility(View.GONE);
                            }
                            if (social_media.contains("FB")) {
                                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                                        StaticUtility.IMG_Facebook) != "") {
                                    imageviewFB.setVisibility(View.VISIBLE);
                                    strFB = SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                                            StaticUtility.IMG_Facebook);
                                    //region ImageFB
                                    try {
                                        URL urla = null;
                                        urla = new URL(strFB);
                                        URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                                        picUrl = String.valueOf(urin.toURL());
                                        // Capture position and set to the ImageView
                                        Picasso.get()
                                                .load(picUrl)
                                                .into(imageviewFB, new Callback() {
                                                    @Override
                                                    public void onSuccess() {
                                                        //holder.pbHome.setVisibility(View.INVISIBLE);
                                                    }

                                                    @Override
                                                    public void onError(Exception e) {
                                                        //holder.pbHome.setVisibility(View.INVISIBLE);
                                                    }
                                                });
                                    } catch (MalformedURLException e) {
                                        e.printStackTrace();
                                        //Creating SendMail object
                                        SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT,
                                                "Getting error in LoginActivity.java When parsing url\n" +
                                                        e.toString());
                                        //Executing sendmail to send email
                                        sm.execute();
                                    } catch (URISyntaxException e) {
                                        e.printStackTrace();
                                        //Creating SendMail object
                                        SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT,
                                                "Getting error in LoginActivity.java When parsing url\n" +
                                                        e.toString());
                                        //Executing sendmail to send email
                                        sm.execute();
                                    }
                                    //endregion
                                } else {
                                    imageviewFB.setVisibility(View.GONE);
                                }
                            } else {
                                imageviewFB.setVisibility(View.GONE);
                            }
                            if (social_media.contains("Google")) {
                                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                                        StaticUtility.IMG_google) != "") {
                                    imageviewGmail.setVisibility(View.VISIBLE);
                                    strGmail = SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                                            StaticUtility.IMG_google);
                                    //region ImageGoogal
                                    try {
                                        URL urla = null;
                                        urla = new URL(strGmail);
                                        URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                                        picUrl = String.valueOf(urin.toURL());
                                        // Capture position and set to the ImageView
                                        Picasso.get()
                                                .load(picUrl)
                                                .into(imageviewGmail, new Callback() {
                                                    @Override
                                                    public void onSuccess() {
                                                        //holder.pbHome.setVisibility(View.INVISIBLE);
                                                    }

                                                    @Override
                                                    public void onError(Exception e) {
                                                        //holder.pbHome.setVisibility(View.INVISIBLE);
                                                    }
                                                });
                                    } catch (MalformedURLException e) {
                                        e.printStackTrace();
                                        //Creating SendMail object
                                        SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT,
                                                "Getting error in LoginActivity.java When parsing url\n" +
                                                        e.toString());
                                        //Executing sendmail to send email
                                        sm.execute();
                                    } catch (URISyntaxException e) {
                                        e.printStackTrace();
                                        //Creating SendMail object
                                        SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT,
                                                "Getting error in LoginActivity.java When parsing url\n" +
                                                        e.toString());
                                        //Executing sendmail to send email
                                        sm.execute();
                                    }
                                    //endregion
                                } else {
                                    imageviewGmail.setVisibility(View.GONE);
                                }
                            } else {
                                imageviewGmail.setVisibility(View.GONE);
                            }
                            if (social_media.contains("Instagram")) {
                                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                                        StaticUtility.IMG_insta) != "") {
                                    imageviewInsta.setVisibility(View.VISIBLE);
                                    strinsta = SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                                            StaticUtility.IMG_insta);
                                    //region ImageInsta
                                    try {
                                        URL urla = null;
                                        urla = new URL(strinsta);
                                        URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                                        picUrl = String.valueOf(urin.toURL());
                                        // Capture position and set to the ImageView
                                        Picasso.get()
                                                .load(picUrl)
                                                .into(imageviewInsta, new Callback() {
                                                    @Override
                                                    public void onSuccess() {
                                                        //holder.pbHome.setVisibility(View.INVISIBLE);
                                                    }

                                                    @Override
                                                    public void onError(Exception e) {
                                                        //holder.pbHome.setVisibility(View.INVISIBLE);
                                                    }
                                                });
                                    } catch (MalformedURLException e) {
                                        e.printStackTrace();
                                        //Creating SendMail object
                                        SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT,
                                                "Getting error in LoginActivity.java When parsing url\n" +
                                                        e.toString());
                                        //Executing sendmail to send email
                                        sm.execute();
                                    } catch (URISyntaxException e) {
                                        e.printStackTrace();
                                        //Creating SendMail object
                                        SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT,
                                                "Getting error in LoginActivity.java When parsing url\n" +
                                                        e.toString());
                                        //Executing sendmail to send email
                                        sm.execute();
                                    }
                                    //endregion
                                } else {
                                    imageviewInsta.setVisibility(View.GONE);
                                }
                            } else {
                                imageviewInsta.setVisibility(View.GONE);
                            }
                        }
                    }
                }else {
                    imageviewFB.setVisibility(View.GONE);
                    imageviewGmail.setVisibility(View.GONE);
                    imageviewInsta.setVisibility(View.GONE);
                    imageviewLinkedin.setVisibility(View.GONE);
                    imageviewPinterest.setVisibility(View.GONE);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        //endregion

        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor) != "") {
            txtForgotpwd.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            txtsignupTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            txtsignup.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            txtSkip.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        }

        chanageEditTextBorder(editUserID);
        chanageEditTextBorder(editUserPWD);
        chanageButton(btnLogin);

        mApp = new InstagramApp(this, StaticUtility.CLIENT_ID,
                StaticUtility.CLIENT_SECRET, StaticUtility.CALLBACK_URL);
        mApp.setListener(new InstagramApp.OAuthAuthenticationListener() {
            @Override
            public void onSuccess() {
                mApp.fetchUserName(handler);

            }

            @Override
            public void onFail(String error) {
                Toast.makeText(LoginActivity.this, error, Toast.LENGTH_SHORT).show();
            }
        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(
                GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        strUSerEmail = getIntent().getStringExtra("userEmail");
        try {
            if (!strUSerEmail.equals("")) {
                editUserID.setText(strUSerEmail);
            } else {
                editUserID.setText("");
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }


    }

    //region FOR GET DEVICE TOKEN...
    private void getDeviceToken() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                sentToken = sharedPreferences.getBoolean(StaticUtility.SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
                    Log.e(TAG, "gcm_send_message");
                } else {
                    Log.e(TAG, "token_error_message");
                }
            }
        };

        //FOR REGISTER RECEIVER...
        registerReceiver();

        //FOR START INTENT-SERVICE TO REGISTER THIS APPLICATION WITH FCM.
        Intent intent = new Intent(LoginActivity.this,
                FirebaseInstanceIDService.class);
        startService(intent);
        /*if (checkPlayServices()) {

        }*/

    }
    //endregion

    //region FOR REGISTER RECEIVER...
    private void registerReceiver() {
        if (!isReceiverRegistered) {
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(StaticUtility.REGISTRATION_COMPLETE));
            isReceiverRegistered = true;
        }
    }
    //endregion

    //region ClickListener
    private void ClickListener() {
        imageviewFB.setOnClickListener(this);
        imageviewInsta.setOnClickListener(this);
        imageviewGmail.setOnClickListener(this);
        imageviewLinkedin.setOnClickListener(this);
        imageviewPinterest.setOnClickListener(this);
        txtsignup.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        txtForgotpwd.setOnClickListener(this);
        txtSkip.setOnClickListener(this);
    }
    //endregion

    //region Initialization
    private void Initialization() {
        imageviewGmail = findViewById(R.id.imageviewGmail);
        imageviewLinkedin = findViewById(R.id.imageviewLinkedin);
        imageviewPinterest = findViewById(R.id.imageviewPinterest);
        imageviewInsta = findViewById(R.id.imageviewInsta);
        imageviewFB = findViewById(R.id.imageviewFB);

        txtsignup = findViewById(R.id.txtsignup);
        txtsignupTitle = findViewById(R.id.txtsignupTitle);
        txtForgotpwd = findViewById(R.id.txtForgotpwd);

        editUserID = (EditText) findViewById(R.id.editUserID);
        editUserEmail = (EditText) findViewById(R.id.editUserEmail);
        editUserPWD = (EditText) findViewById(R.id.editUserPWD);

        btnLogin = findViewById(R.id.btnLogin);
        imageLogo = findViewById(R.id.imageLogo);

        relativeProgress = findViewById(R.id.relativeProgress);

        coordinator = findViewById(R.id.coordinator);

        txtSkip = findViewById(R.id.txtSkip);

    }
    //endregion

    //region Typeface
    public void Typeface() {
        editUserID.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editUserPWD.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtForgotpwd.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnLogin.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtsignupTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtsignup.setTypeface(Typefaces.TypefaceCalibri_bold(context));
        txtSkip.setTypeface(Typefaces.TypefaceCalibri_bold(context));
    }
    //endregion

    //region ON PAUSE...
    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        isReceiverRegistered = false;
        super.onPause();
    }
    //endregion

    private void setDynamicString() {
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEMAIL_ADDRESS) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEMAIL_ADDRESS).equals("")) {
                String str = SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEMAIL_ADDRESS);
                editUserID.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEMAIL_ADDRESS));
            } else {
                editUserID.setHint(getString(R.string.email));
            }
        } else {
            editUserID.setHint(getString(R.string.email));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPASSWORD) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPASSWORD).equals("")) {
                editUserPWD.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPASSWORD));
            } else {
                editUserPWD.setHint(getString(R.string.password));
            }
        } else {
            editUserPWD.setHint(getString(R.string.password));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sFORGOT_YOUR_PASSWORD) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sFORGOT_YOUR_PASSWORD).equals("")) {
                txtForgotpwd.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sFORGOT_YOUR_PASSWORD));
            } else {
                txtForgotpwd.setText(getString(R.string.forgot_pwd));
            }
        } else {
            txtForgotpwd.setText(getString(R.string.forgot_pwd));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOG_IN) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOG_IN).equals("")) {
                btnLogin.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOG_IN));
            } else {
                btnLogin.setText(getString(R.string.login));
            }
        } else {
            btnLogin.setText(getString(R.string.login));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOT_HAVE_AN_ACCOUNT) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOT_HAVE_AN_ACCOUNT).equals("")) {
                txtsignupTitle.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOT_HAVE_AN_ACCOUNT));
            } else {
                txtsignupTitle.setText(getString(R.string.dontsignup));
            }
        } else {
            txtsignupTitle.setText(getString(R.string.dontsignup));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSIGN_UP_NOW) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSIGN_UP_NOW).equals("")) {
                txtsignup.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSIGN_UP_NOW));
            } else {
                txtsignup.setText(getString(R.string.signup));
            }
        } else {
            txtsignup.setText(getString(R.string.signup));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSKIP) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSKIP).equals("")) {
                txtSkip.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSKIP) + " >>");
            } else {
                txtSkip.setText(getString(R.string.skip));
            }
        } else {
            txtSkip.setText(getString(R.string.skip));
        }
    }

    @Override
    public void onBackPressed() {
        /*super.onBackPressed();*/
        if (strRedirect != null && strRedirect.equalsIgnoreCase("myaccount")) {
            MainActivity.manageBackPress(true);
            Intent intent = new Intent(context, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            /*finish();*/
        } else if (strRedirect != null && strRedirect.equalsIgnoreCase("logout")) {
            MainActivity.manageBackPress(true);
            Intent intent = new Intent(context, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageviewGmail:
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;
            case R.id.imageviewInsta:
                mApp.authorize();
                break;
            case R.id.txtsignup:
                startActivity(new Intent(context, RegistrationActivity.class).putExtra("redirect",strRedirect));
                break;
            case R.id.txtForgotpwd:
                startActivity(new Intent(context, ForgotPasswordActivity.class));
                break;
            case R.id.txtSkip:
                if (strRedirect != null && strRedirect.equalsIgnoreCase("myaccount")) {
                    MainActivity.manageBackPress(true);
                    Intent intent = new Intent(context, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    /*finish();*/
                } else if (strRedirect != null && strRedirect.equalsIgnoreCase("logout")) {
                    MainActivity.manageBackPress(true);
                    Intent intent = new Intent(context, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    finish();
                }
                break;

            case R.id.imageviewLinkedin:
                loginLinkedin();
                break;

            case R.id.imageviewPinterest:
                PinterestAuthentication();
                break;

            case R.id.btnLogin:
                InputMethodManager imm = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(
                        btnLogin.getWindowToken(), 0);
                sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                sentToken = sharedPreferences.getBoolean(StaticUtility.SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
                    token = sharedPreferences.getString(StaticUtility.GCM_TOKEN, "");
                    if (Validation()) {
                        LoginAPI(token);
                    }
                } else {
                    Toast.makeText(context, "Connect Your internet!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        pdkClient.onOauthResponse(requestCode, resultCode, data);
        LISessionManager.getInstance(getApplicationContext())
                .onActivityResult(this,
                        requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else if (requestCode == 1) {
            if (!Global.isNetworkAvailable(context)) {
                openInternetAlertDialog(context, "Not connected to Internet");
            } else {
                internetAlert.dismiss();
            }
        }
    }

    //region For handel gmail sign In
    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            //Log.e(TAG, "display name: " + acct.getDisplayName());

            user_name = acct.getDisplayName();
            userEmail = acct.getEmail();
            gmailId = acct.getId();
            social_type = "google";
            CheckSocialRegister(gmailId, social_type, user_name, userEmail);
        } else {
            Toast.makeText(context, "Unable to fetch data, Proceed manually", Toast.LENGTH_SHORT).show();
        }
    }
    //endregion

    //region SOCIAL MEDIA
    public void CheckSocialRegister(final String social_id, final String Social_type,
                                    final String userName, final String userEmail) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"social_id", "social_type", "email_id"};
        String[] val = {social_id, Social_type, userEmail};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.Checkregister);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                if (strStatus.equals("ok")) {
                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                    JSONObject jsonObject = response.getJSONObject("payload");
                                    String user_id = jsonObject.getString("user_id");
                                    String email = jsonObject.getString("email");
                                    String firstname = jsonObject.getString("firstname");

                                    JSONObject jsonObjectAuther = jsonObject.getJSONObject("authUser");
                                    String user_token = jsonObjectAuther.getString("user_token");

                                    Intent intentMain = new Intent(context, MainActivity.class);
                                    SharedPreference.CreatePreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.SavePreference(Global.USERID, user_id);
                                    SharedPreference.SavePreference(Global.USERTOKEN, user_token);
                                    SharedPreference.SavePreference(Global.USER_EMAIL, email);
                                    SharedPreference.SavePreference(Global.USER_Name, firstname);
                                    startActivity(intentMain);
                                    finish();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                relativeProgress.setVisibility(View.GONE);
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                String str = null, struserEmail = null, strSocialType = null, strSocialId = null;
                                str = userName;
                                struserEmail = userEmail;
                                strSocialType = Social_type;
                                strSocialId = social_id;
                                String[] str1 = str.split(" ");
                                if (str1.length > 1) {
                                    Intent intent = new Intent(context, RegistrationActivity.class);
                                    intent.putExtra("firstname", str1[0]);
                                    intent.putExtra("lastname", str1[1]);
                                    intent.putExtra("Useremail", struserEmail);
                                    intent.putExtra("SocialType", strSocialType);
                                    intent.putExtra("SocialId", strSocialId);
                                    startActivity(intent);
                                } else {
                                    Intent intent = new Intent(context, RegistrationActivity.class);
                                    intent.putExtra("firstname", str1[0]);
                                    intent.putExtra("lastname", "");
                                    intent.putExtra("Useremail", struserEmail);
                                    intent.putExtra("SocialType", strSocialType);
                                    intent.putExtra("SocialId", strSocialId);
                                    startActivity(intent);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in LoginActivity.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver();
        callbackManager = CallbackManager.Factory.create();
        login_button = (LoginButton) findViewById(R.id.login_button);
        assert login_button != null;
        login_button.setReadPermissions("email");

        Intent intent = getIntent();
        String email = intent.getStringExtra("email");
        editUserEmail.setText(email);

        imageviewFB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //progressDialog = new ProgressDialog(Login.this);
                //progressDialog.setMessage("Loading...");
                //progressDialog.show();

                login_button.performClick();

                login_button.setPressed(true);

                login_button.invalidate();

                login_button.registerCallback(callbackManager, mCallBack);

                login_button.setPressed(false);

                login_button.invalidate();

            }
        });
        callbackManager = CallbackManager.Factory.create();

    }

    //region compute Pakage KeyHash
    private void computePakageHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    context.getPackageName(),  // replace with your unique package name
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
//                String something = new String(Base64.encode(md.digest(), 0));
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException ignored) {
            ignored.printStackTrace();
            /*//Creating SendMail object
            SendMail sm = new SendMail(this, Global.TOEMAIL, Global.SUBJECT, e.toString());
            //Executing sendmail to send email
            sm.execute();*/
        }
    }
    //endregion

    //region Linkedin Social media intergration
    public void loginLinkedin() {
        final String url = "https://api.linkedin.com/v1/people/~:" +
                "(id,first-name,last-name,email-address,formatted-name,phone-numbers," +
                "picture-urls::(original))";
        LISessionManager.getInstance(getApplicationContext())
                .init(this, buildScope(), new AuthListener() {
                    @Override
                    public void onAuthSuccess() {

                        /*Toast.makeText(getApplicationContext(), "success" +
                                        LISessionManager
                                                .getInstance(getApplicationContext())
                                                .getSession().getAccessToken().toString(),
                                Toast.LENGTH_LONG).show();*/
                        APIHelper apiHelper = APIHelper.getInstance(getApplicationContext());
                        apiHelper.getRequest(LoginActivity.this, url, new ApiListener() {
                            @Override
                            public void onApiSuccess(ApiResponse result) {
                                try {
                                    JSONObject jsonObject = result.getResponseDataAsJson();
                                    String Fname = jsonObject.optString("firstName");
                                    String Lname = jsonObject.optString("lastName");
                                    String email = jsonObject.optString("emailAddress");
                                    String Social_id = jsonObject.optString("id");
                                    String Picture = jsonObject.optString("pictureUrl");
                                    social_type = "linkedin";
                                    CheckSocialRegister(Social_id,
                                            social_type, Fname + " " + Lname, email);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onApiError(LIApiError error) {
                                error.printStackTrace();
                            }
                        });

                    }

                    @Override
                    public void onAuthError(LIAuthError error) {

                       /* Toast.makeText(getApplicationContext(), "failed "
                                        + error.toString(),
                                Toast.LENGTH_LONG).show();*/

                        /*Toast.makeText(getApplicationContext(), "Please Try Again..!",
                                Toast.LENGTH_LONG).show();*/
                    }
                }, true);
    }

    private static Scope buildScope() {
        return Scope.build(Scope.R_BASICPROFILE, Scope.R_EMAILADDRESS, Scope.W_SHARE);
    }
    //endregion

    //region Pinterest Authentication
    private void PinterestAuthentication() {
        List scopes = new ArrayList<String>();
        scopes.add(PDKClient.PDKCLIENT_PERMISSION_READ_PUBLIC);
        scopes.add(PDKClient.PDKCLIENT_PERMISSION_WRITE_PUBLIC);
        scopes.add(PDKClient.PDKCLIENT_PERMISSION_READ_RELATIONSHIPS);
        scopes.add(PDKClient.PDKCLIENT_PERMISSION_WRITE_RELATIONSHIPS);
        scopes.add(PDKClient.PDKCLIENT_PERMISSION_READ_PRIVATE);
        scopes.add(PDKClient.PDKCLIENT_PERMISSION_WRITE_PRIVATE);

        pdkClient.login(this, scopes, new PDKCallback() {
            @Override
            public void onSuccess(PDKResponse response) {
                finish();
                Log.d(getClass().getName(), response.getData().toString());
                getMe();
               /* String Fname = response.getUser().getFirstName();
                String Lname = response.getUser().getLastName();
                String Email = response.getUser().getUsername();
                String ID = response.getUser().getUid();
                CheckSocialRegister(ID, "Pinterest", Fname + " " + Lname, Email);*/
            }

            @Override
            public void onFailure(PDKException exception) {
                Log.e(getClass().getName(), exception.getDetailMessage());
                /*Toast.makeText(getApplicationContext(), exception.getDetailMessage() + "Please Try Again..!",
                        Toast.LENGTH_LONG).show();*/
            }
        });

    }
    //endregion

    //region get User Details
    private void getMe() {
        PDKClient.getInstance().getMe(USER_FIELDS, new PDKCallback() {
            @Override
            public void onSuccess(PDKResponse response) {
                if (DEBUG) log(String.format("status: %d", response.getStatusCode()));
                user = response.getUser();
                String Fname = user.getFirstName();
                String Lname = user.getLastName();
                String Email = user.getUsername();
                String ID = user.getUid();
                CheckSocialRegister(ID, "Pinterest", Fname + " " + Lname, Email);
            }

            @Override
            public void onFailure(PDKException exception) {
                /*if (DEBUG) log(exception.getDetailMessage());*/
                /*Toast.makeText(LoginActivity.this, "/me Request failed", Toast.LENGTH_SHORT).show();*/
            }
        });
    }//endregion

    private void log(String msg) {
        if (!Utils.isEmpty(msg))
            Log.d(getClass().getName(), msg);
    }


    //region For FacebookWith Login
    private FacebookCallback<LoginResult> mCallBack = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {

            //progressDialog.dismiss();

            // App code
            //Profile profile = Profile.getCurrentProfile();
            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            Log.e("response: ", response + "");
                            try {
                                //String str = object.toString();
                                user_fb_email = object.getString("email");
                                user_fb_Id = object.getString("id");
                                user_name = object.getString("name");
                                social_type = "facebook";
                                CheckSocialRegister(user_fb_Id, social_type, user_name, user_fb_email);
                                /*user = new User();
                                user.facebookID = object.getString("id");
                                //user.email = object.getString("email");
                                //user.email="atlmavani@ymail.com";
                                user.name = object.getString("name");
                                user.gender = object.getString("gender");
                                PrefUtils.setCurrentUser(user,Login.this);*/

                            } catch (Exception e) {
                                e.printStackTrace();
                                //Creating SendMail object
                                SendMail sm = new SendMail(LoginActivity.this, Global.TOEMAIL, Global.SUBJECT, "Getting error in Register.java When parsing response from facebook.\n" + e.toString());
                                //Executing sendmail to send email
                                sm.execute();
                            }
                            /*TimeStamp=Security.getCurrentTimeStamp();
                            ipv4=Security.getIPAddress(true);
                            //SaltMd5=Security.md5(TimeStamp);
                            // SaltMd5="1";
                            //encryptedData=Security.encrypt(SaltMd5);
                            encryptedData=Security.tokenencrypt(TimeStamp);
                            //decryptedData=Security.decrypt(encryptedData, Salt);
                            String username[]=user.name.split(" ");
                            data.put("firstname", username[0]);
                            data.put("lastname", username[1]);
                            data.put("email", "");
                            data.put("gender", user.gender);
                            data.put("dob", " ");
                            data.put("mobile", " ");
                            data.put("current_city", " ");
                            data.put("fb_id", user.facebookID);
                            data.put("g_id", " ");
                            data.put("profile_img", " ");
                            data.put("occupation", " ");
                            data.put("time_stamp", TimeStamp);
                            data.put("token", encryptedData);
                            data.put("client_ip", ipv4);
                            new UserLogin().execute();*/
                        }

                    });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,gender, birthday");
            request.setParameters(parameters);
            request.executeAsync();
        }

        @Override
        public void onCancel() {
            //progressDialog.dismiss();
        }

        @Override
        public void onError(FacebookException e) {
            e.printStackTrace();
            //progressDialog.dismiss();
            //Creating SendMail object
            SendMail sm = new SendMail(LoginActivity.this, Global.TOEMAIL, Global.SUBJECT, "Getting error in Register.java When parsing response from facebook.\n" + e.toString());
            //Executing sendmail to send email
            sm.execute();
        }
    };
    //endregion

    //region Instagram
    private Handler handler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == InstagramApp.WHAT_FINALIZE) {
                userInfoHashmap = mApp.getUserInfo();
                instagram_Id = userInfoHashmap.get("id");
                user_inta_Username = userInfoHashmap.get("username");
                user_insta_fullname = userInfoHashmap.get("full_name");
                social_type = "instagram";
                CheckSocialRegister(instagram_Id, social_type, user_insta_fullname, "");
            } else if (msg.what == InstagramApp.WHAT_FINALIZE) {
                Toast.makeText(LoginActivity.this, "Check your network.",
                        Toast.LENGTH_SHORT).show();
            }
            return false;
        }
    });
    //endregion

    //region Validation
    public Boolean Validation() {
        Boolean valid = false;
        if (!TextUtils.isEmpty(editUserID.getText())) {
            editUserID.setError(null);
            if (Global.isValidEmail(editUserID.getText())) {
                if (!TextUtils.isEmpty(editUserPWD.getText())) {
                    editUserPWD.setError(null);
                    if (editUserPWD.length() >= 6) {
                        editUserPWD.setError(null);
                        valid = true;
                    } else {
                        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPASSWORD_LIMIT_VALID) != null) {
                            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPASSWORD_LIMIT_VALID).equals("")) {
                                editUserPWD.setError(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPASSWORD_LIMIT_VALID));
                            } else {
                                editUserPWD.setError(getString(R.string.valid_password_error_message));
                            }
                        } else {
                            editUserPWD.setError(getString(R.string.valid_password_error_message));
                        }
                    }
                } else {
                    if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPASSWORD_CHECK_EMPTY) != null) {
                        if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPASSWORD_CHECK_EMPTY).equals("")) {
                            editUserPWD.setError(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPASSWORD_CHECK_EMPTY));
                        } else {
                            editUserPWD.setError(getString(R.string.null_password_error_message));
                        }
                    } else {
                        editUserPWD.setError(getString(R.string.null_password_error_message));
                    }
                }
            } else {
                if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEMAIL_CHECK_VALID) != null) {
                    if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEMAIL_CHECK_VALID).equals("")) {
                        editUserID.setError(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEMAIL_CHECK_VALID));
                    } else {
                        editUserID.setError(getString(R.string.valid_email_error_message));
                    }
                } else {
                    editUserID.setError(getString(R.string.valid_email_error_message));
                }
            }
        } else {
            if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEMAIL_CHECK_EMPTY) != null) {
                if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEMAIL_CHECK_EMPTY).equals("")) {
                    editUserID.setError(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEMAIL_CHECK_EMPTY));
                } else {
                    editUserID.setError(getString(R.string.null_email_error_message));
                }
            } else {
                editUserID.setError(getString(R.string.null_email_error_message));
            }
        }
        return valid;
    }
    //endregion

    //region FOR Loging API...
    private void LoginAPI(String token) {
        relativeProgress.setVisibility(View.VISIBLE);
        String strCurrencyCode = SharedPreference.GetPreference(context, Global.PREFERENCECURRENCY, StaticUtility.sCurrencyCode);
        String[] key = {"email", "password", "device_id", "currencyCode"};
        String[] val = {editUserID.getText().toString(), editUserPWD.getText().toString(),
                token, strCurrencyCode};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(
                StaticUtility.URL + StaticUtility.Login + Global.queryStringUrl(context));
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                               String strStatus = response.getString("status");
                                String strMessage = response.optString("message");
                                if (strStatus.equals("ok")) {
                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                    JSONObject jsonObject = response.optJSONObject("payload");
                                    SharedPreference.CreatePreference(context, StaticUtility.PREFERENCEREFERRALCODE);
                                    SharedPreference.SavePreference(StaticUtility.sREFERRALCODE, jsonObject.optString("referral_code"));
                                    String user_id = jsonObject.getString("user_id");
                                    String email = jsonObject.getString("email");
                                    String firstname = jsonObject.getString("firstname");
                                    JSONObject jsonObjectAuther = jsonObject.optJSONObject("authUser");
                                    String user_token = jsonObjectAuther.optString("user_token");
                                    Object object = jsonObject.opt("profilepicture");
                                    String profilepicture = "";
                                    if (!object.equals("")) {
                                        JSONArray jsonArrayProfilePicture = jsonObject.getJSONArray("profilepicture");
                                        profilepicture = (String) jsonArrayProfilePicture.get(0);
                                    }

                                    SharedPreference.CreatePreference(context, StaticUtility.PREFERENCEREFERRALCODE);
                                    SharedPreference.SavePreference(StaticUtility.sREFERRALCODE, jsonObject.optString("ref_code"));

                                    if (jsonObject.has("currencydata")) {
                                        JSONObject jsonObjectCurrencyData = jsonObject.optJSONObject("currencydata");
                                        String strZcurrencyidPk = jsonObjectCurrencyData.optString("z_currencyid_pk");
                                        String strCurrencyId = jsonObjectCurrencyData.optString("currency_id");
                                        String strCurrency = jsonObjectCurrencyData.optString("currency");
                                        String strCode = jsonObjectCurrencyData.optString("code");
                                        String strFlag = jsonObjectCurrencyData.optString("flag");
                                        String strSymbol = jsonObjectCurrencyData.optString("symbol");
                                        String strPostOrPreText = jsonObjectCurrencyData.optString("post_or_pre_text");
                                        String strIsBasecurrency = jsonObjectCurrencyData.optString("is_basecurrency");
                                        String strIsActive = jsonObjectCurrencyData.optString("is_active");
                                        String strZcurrencyrateidPk = jsonObjectCurrencyData.optString("z_currencyrateid_pk");
                                        String strCurrencyRateId = jsonObjectCurrencyData.optString("currency_rate_id");
                                        String strZcurrencyidFk = jsonObjectCurrencyData.optString("z_currencyid_fk");
                                        String strRate = jsonObjectCurrencyData.optString("rate");

                                        SharedPreference.CreatePreference(context, Global.PREFERENCECURRENCY);
                                        SharedPreference.SavePreference(StaticUtility.sCurrencyName, strCurrency);
                                        SharedPreference.SavePreference(StaticUtility.sCurrencySign, strSymbol);
                                        SharedPreference.SavePreference(StaticUtility.sCurrencySignPosition, strPostOrPreText);
                                        SharedPreference.SavePreference(StaticUtility.sCurrencyCode, strCode);
                                    }

                                    SharedPreference.CreatePreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.SavePreference(Global.USERID, user_id);
                                    SharedPreference.SavePreference(Global.USERTOKEN, user_token);
                                    SharedPreference.SavePreference(Global.USER_EMAIL, email);
                                    SharedPreference.SavePreference(Global.USER_Name, firstname);
                                    SharedPreference.SavePreference(Global.USER_Profile_Picture, profilepicture);
                                    GuestAPI();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                relativeProgress.setVisibility(View.GONE);
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in LoginActivity.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR GuestAPI API...
    private void GuestAPI() {
        relativeProgress.setVisibility(View.VISIBLE);

        String session_id = SharedPreference.GetPreference(context, Global.preferenceNameGuestUSer, Global.SessionId);

        String[] key = {"session_id"};
        String[] val = {session_id};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.UPDATE_CART_BY_USERID_AFTER_LOGIN);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(context));
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
//                                    SharedPreference.ClearPreference(context, Global.preferenceNameGuestUSer);
                                    String strUserName = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USER_Name);
                                    String strUserEmail = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USER_EMAIL);
                                    String strProfile_Image = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USER_Profile_Picture);
                                    /*MainActivity.SetUserInfo();*/
                                    if (strRedirect != null && strRedirect.equalsIgnoreCase("myaccount")) {
                                        MainActivity.manageBackPress(true);
                                        Intent intent = new Intent(context, MainActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        /*finish();*/
                                    } else if (strRedirect != null && strRedirect.equalsIgnoreCase("logout")) {
                                        MainActivity.manageBackPress(true);
                                        Intent intent = new Intent(context, MainActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    } else {
                                        finish();
                                    }

                                    /*startActivity(new Intent(context, MainActivity.class));
                                    finish();*/
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                relativeProgress.setVisibility(View.GONE);
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in LoginActivity.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region chanageEditTextBorder
    public void chanageEditTextBorder(EditText editText) {
        editText.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editText.setBackgroundResource(R.drawable.ic_border);
        GradientDrawable gd = (GradientDrawable) editText.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        /*gd.setColor(Color.parseColor("#000000"));*/
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(60);
        gd.setStroke(2, Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        gd.setColor(Color.parseColor("#FFFFFF"));

    }
    //endregion

    //region chanageButton
    public void chanageButton(Button button) {
        button.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
//        button.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TEXTCOLOR)));
        button.setBackgroundResource(R.drawable.ic_button);
        GradientDrawable gd = (GradientDrawable) button.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
// gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(60);
        /* gd.setStroke(2, Color.parseColor(StaticUtility.BORDERCOLOR));*/
    }
    //endregion

    //region For EventBus onEvent
    public void onEvent(String event) {
        openInternetAlertDialog(context, event);
    }
    //endregion

    //region FOR SHOW INTERNET CONNECTION DIALOG...
    public void openInternetAlertDialog(final Context mContext, String alertString) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View row = inflater.inflate(R.layout.row_alert_dialog, null);
        final TextView tvAlertText = row.findViewById(R.id.tvAlertText);
        final TextView tvTitle = row.findViewById(R.id.tvTitle);
        final Button btnSettings = row.findViewById(R.id.btnSettings);
        final Button btnExit = row.findViewById(R.id.btnExit);

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDATE_OFF) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDATE_OFF).equals("")) {
                tvTitle.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDATE_OFF));
            } else {
                tvTitle.setText(getText(R.string.your_data_is_off));
            }
        } else {
            tvTitle.setText(getText(R.string.your_data_is_off));
        }

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTURN_ON_DATA_WIFI) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTURN_ON_DATA_WIFI).equals("")) {
                tvAlertText.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTURN_ON_DATA_WIFI));
            } else {
                tvAlertText.setText(getText(R.string.turn_on_data_or_wi_fi_in_nsettings));
            }
        } else {
            tvAlertText.setText(getText(R.string.turn_on_data_or_wi_fi_in_nsettings));
        }

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSETTINGS) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSETTINGS).equals("")) {
                btnSettings.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSETTINGS));
            } else {
                btnSettings.setText(getText(R.string.settings));
            }
        } else {
            btnSettings.setText(getText(R.string.settings));
        }

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXIT) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXIT).equals("")) {
                btnExit.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXIT));
            } else {
                btnExit.setText(getText(R.string.exit));
            }
        } else {
            btnExit.setText(getText(R.string.exit));
        }

        tvTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        tvAlertText.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        btnSettings.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        btnExit.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        btnExit.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        btnSettings.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        tvTitle.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));

        tvAlertText.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        tvTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnSettings.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnExit.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

        /*tvAlertText.setText("Turn on data or WI-FI in \n settings.");*/

        if (alertString.equals("Not connected to Internet")) {
            if (i == 0) {
                i = 1;
                AlertDialog.Builder i_builder = new AlertDialog.Builder(mContext);
                internetAlert = i_builder.create();
                internetAlert.setCancelable(false);
                internetAlert.setView(row);

                if (internetAlert.isShowing()) {
                    internetAlert.dismiss();
                } else {
                    internetAlert.show();
                }

                btnExit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        internetAlert.dismiss();
                        //FOR CLOSE APP...
                        System.exit(0);
                    }
                });

                btnSettings.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        /*internetAlert.dismiss();*/
                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                    }
                });
            } else {
                /*internetAlert.dismiss();*/
            }
        } else {
            i = 0;
            internetAlert.dismiss();
        }
    }
    //endregion

}
