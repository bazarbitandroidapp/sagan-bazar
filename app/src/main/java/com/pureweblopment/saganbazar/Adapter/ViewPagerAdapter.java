package com.pureweblopment.saganbazar.Adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.pureweblopment.saganbazar.Global.Global;
import com.pureweblopment.saganbazar.Global.SharedPreference;
import com.pureweblopment.saganbazar.Global.StaticUtility;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;

import com.pureweblopment.saganbazar.Global.SendMail;
import com.pureweblopment.saganbazar.Model.Sliders;
import com.pureweblopment.saganbazar.R;

/**
 * Created by Admin on 24-04-2017.
 */

public class ViewPagerAdapter extends PagerAdapter {
    // Declare Variables
    Context context;
    LayoutInflater inflater;
    String[] slider;

    ArrayList<Sliders> sliderses = new ArrayList<>();

    public ViewPagerAdapter(Context context, String[] flag) {
        this.context = context;
        this.slider = flag;
    }

    public ViewPagerAdapter(Context context, ArrayList<Sliders> sliderses) {
        this.context = context;
        this.sliderses = sliderses;
    }

    @Override
    public int getCount() {
        return slider.length;
//        return sliderses.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        final ImageView img_slider;
        final RelativeLayout rlImgHolder;
        final ProgressBar pbImgHolder;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.viewpager_slider, container, false);
//        Sliders sliders = sliderses.get(position);

        // Locate the ImageView in viewpager_item.xml
        img_slider = (ImageView) itemView.findViewById(R.id.img_slider);
        rlImgHolder = (RelativeLayout) itemView.findViewById(R.id.rlImgHolder);
        pbImgHolder = (ProgressBar) itemView.findViewById(R.id.pbImgHolder);
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                pbImgHolder.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
            }
        }
        String picUrl = null;
        try {
            URL urla = null;
            urla = new URL(slider[position].replaceAll("%20", " "));
            URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
            picUrl = String.valueOf(urin.toURL());
            // Capture position and set to the ImageView
            Picasso.get()
                    .load(picUrl)
                    .into(img_slider, new Callback() {
                        @Override
                        public void onSuccess() {
                            //holder.pbHome.setVisibility(View.INVISIBLE);
                            rlImgHolder.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(Exception e) {
                            //holder.pbHome.setVisibility(View.INVISIBLE);
                        }
                    });
        } catch (MalformedURLException e) {
            e.printStackTrace();
            //Creating SendMail object
            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing url\n" + e.toString());
            //Executing sendmail to send email
            sm.execute();
        } catch (URISyntaxException e) {
            e.printStackTrace();
            //Creating SendMail object
            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing url\n" + e.toString());
            //Executing sendmail to send email
            sm.execute();
        }
        // Add viewpager_item.xml to ViewPager
        ((ViewPager) container).addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove viewpager_item.xml from ViewPager
        ((ViewPager) container).removeView((RelativeLayout) object);

    }
}