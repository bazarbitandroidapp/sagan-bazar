package com.pureweblopment.saganbazar.Linkedin.internals;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;

import com.pureweblopment.saganbazar.Global.SharedPreference;
import com.pureweblopment.saganbazar.Global.StaticUtility;
import com.pureweblopment.saganbazar.R;


public class AppStore {
    static String strMessage = "";
    static String strTitle = "";
    static String strPositive = "";
    static String strNegative = "";

    public static void goAppStore(final Activity activity, boolean showDialog) {

        if (SharedPreference.GetPreference(activity, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCONNECT_LINKEDIN) != null) {
            if (!SharedPreference.GetPreference(activity, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCONNECT_LINKEDIN).equals("")) {
                strMessage = SharedPreference.GetPreference(activity, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCONNECT_LINKEDIN);
            } else {
                strMessage = (activity.getString(R.string.update_linkedin_app_message));
            }
        } else {
            strMessage = (activity.getString(R.string.update_linkedin_app_message));
        }
        if (SharedPreference.GetPreference(activity, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sUPDATE_LINKEDIN) != null) {
            if (!SharedPreference.GetPreference(activity, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sUPDATE_LINKEDIN).equals("")) {
                strTitle = SharedPreference.GetPreference(activity, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sUPDATE_LINKEDIN);
            } else {
                strTitle = (activity.getString(R.string.update_linkedin_app_title));
            }
        } else {
            strTitle = (activity.getString(R.string.update_linkedin_app_title));
        }
        if (SharedPreference.GetPreference(activity, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDOWNLOAD) != null) {
            if (!SharedPreference.GetPreference(activity, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDOWNLOAD).equals("")) {
                strPositive = SharedPreference.GetPreference(activity, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDOWNLOAD);
            } else {
                strPositive = (activity.getString(R.string.update_linkedin_app_title));
            }
        } else {
            strPositive = (activity.getString(R.string.update_linkedin_app_title));
        }
        if (SharedPreference.GetPreference(activity, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCEL) != null) {
            if (!SharedPreference.GetPreference(activity, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCEL).equals("")) {
                strNegative = SharedPreference.GetPreference(activity, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCEL);
            } else {
                strNegative = (activity.getString(R.string.update_linkedin_app_title));
            }
        } else {
            strNegative = (activity.getString(R.string.update_linkedin_app_title));
        }
        if (!showDialog) {
            goToAppStore(activity);
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(strMessage)
                .setTitle(strTitle);
        builder.setPositiveButton(strPositive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                goToAppStore(activity);
                dialogInterface.dismiss();
            }
        });
        builder.setNegativeButton(strNegative, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.create().show();
    }

    private static void goToAppStore(final Activity activity) {
        SupportedAppStore appStore = SupportedAppStore.fromDeviceManufacturer();
        String appStoreUri = appStore.getAppStoreUri();
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(appStoreUri));
        try {
            activity.startActivity(intent);
        } catch (android.content.ActivityNotFoundException e) {
            //should not happen
        }
    }

    private static enum SupportedAppStore {
        amazonAppstore("amazon", "amzn://apps/android?p=com.linkedin.android"),
        googlePlay("google", "market://details?id=com.linkedin.android"),
        samsungApps("samsung", "samsungapps://ProductDetail/com.linkedin.android");

        private final String deviceManufacturer;
        private final String appStoreUri;

        private SupportedAppStore(String deviceManufacturer, String appStoreUri) {
            this.deviceManufacturer = deviceManufacturer;
            this.appStoreUri = appStoreUri;
        }

        public String getDeviceManufacturer() {
            return deviceManufacturer;
        }

        public String getAppStoreUri() {
            return appStoreUri;
        }

        public static SupportedAppStore fromDeviceManufacturer() {
            for (SupportedAppStore appStore : values()) {
                if (appStore.getDeviceManufacturer().equalsIgnoreCase(Build.MANUFACTURER)) {
                    return appStore;
                }
            }
            //return google play by default
            return googlePlay;
        }
    }

    ;

}
