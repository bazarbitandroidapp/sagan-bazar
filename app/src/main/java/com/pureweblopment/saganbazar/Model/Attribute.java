package com.pureweblopment.saganbazar.Model;

/**
 * Created by divya on 11/9/17.
 */

public class Attribute {
    String attributetermsname;
    String attributeterm_id;
    String isSelect;

    public Attribute(String attributetermsname, String attributeterm_id) {
        this.attributetermsname = attributetermsname;
        this.attributeterm_id = attributeterm_id;
    }

    public String getAttributetermsname() {
        return attributetermsname;
    }

    public String getAttributeterm_id() {
        return attributeterm_id;
    }

    public String getIsSelect() {
        return isSelect;
    }

    public void setIsSelect(String isSelect) {
        this.isSelect = isSelect;
    }
}
