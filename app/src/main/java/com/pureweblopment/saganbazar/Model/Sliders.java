package com.pureweblopment.saganbazar.Model;

/**
 * Created by divya on 4/8/17.
 */

public class Sliders {
    int images;
    String imageurl;

    public Sliders(int images, String imageurl) {
        this.images = images;
        this.imageurl = imageurl;
    }

    public Sliders(int images) {
        this.images = images;
    }

    public Sliders(String imageurl) {
        this.imageurl = imageurl;
    }

    public int getImages() {
        return images;
    }

    public String getImageurl() {
        return imageurl;
    }
}

