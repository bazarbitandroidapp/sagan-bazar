package com.pureweblopment.saganbazar.Model;

import java.io.Serializable;

/**
 * Created by divya on 29/7/17.
 */

public class SpinnerItem implements Serializable {

    public int value;
    public String countryname;
    public String countrycode;

    public SpinnerItem(String countryname, String countrycode, int value) {
        this.countryname = countryname;
        this.countrycode = countrycode;
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public String getCountryname() {
        return countryname;
    }

    public String getCountrycode() {
        return countrycode;
    }
}
