package com.pureweblopment.saganbazar.Model;

import org.json.JSONArray;

/**
 * Created by divya on 15/9/17.
 */

public class Variations {
    String Slug;
    String VariationName;
    String Price;
    String Sale_Price;
    String product_id;
    JSONArray main_image;
    JSONArray gallery_images;
    String productid;
    String attributeid;

    public Variations(String product_id, String variationName) {
        this.product_id = product_id;
        this.VariationName = variationName;
    }

    public Variations(String slug, String variationName, String price,
                      String sale_Price, JSONArray main_image, JSONArray gallery_images, String product_id) {
        Slug = slug;
        VariationName = variationName;
        Price = price;
        Sale_Price = sale_Price;
        this.main_image = main_image;
        this.gallery_images = gallery_images;
        this.product_id = product_id;
    }

    public String getSlug() {
        return Slug;
    }

    public String getVariationName() {
        return VariationName;
    }

    public String getPrice() {
        return Price;
    }

    public String getSale_Price() {
        return Sale_Price;
    }

    public JSONArray getMain_image() {
        return main_image;
    }

    public JSONArray getGallery_images() {
        return gallery_images;
    }

    public String getProduct_id() {
        return product_id;
    }

    public String getProductid() {
        return productid;
    }

    public String getAttributeid() {
        return attributeid;
    }
}
